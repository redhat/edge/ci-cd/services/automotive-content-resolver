# Automotive Content Resolver Input

This repository contains the Automotive configurations for Content Resolver.

See [config specs](https://github.com/minimization/content-resolver/tree/master/config_specs) for reference.

# Exploring the Results

You can browse the latest generated output of the default branch (main) by visiting the [GitLab Pages](https://redhat.gitlab.io/edge/ci-cd/services/automotive-content-resolver/) and clicking Explore results.

This will take you to the [Workloads view](https://redhat.gitlab.io/edge/ci-cd/services/automotive-content-resolver/workloads.html) which shows the different generated workloads associated to the config files.

# CI/CD

The GitLab CI will validate MRs changing the config files by running content-resolver and validating that it runs successfully.

The output can be browsed by looking at the test artifacts. See []() for reference.

On merge to the default branch (main), the content-resolver output will be generated and deployed to the [GitLab Pages](https://redhat.gitlab.io/edge/ci-cd/services/automotive-content-resolver/) for this repo.

A scheduled run of the pipeline every 6 hours will update the content-resolver output hosted on the [GitLab Pages](https://redhat.gitlab.io/edge/ci-cd/services/automotive-content-resolver/).

